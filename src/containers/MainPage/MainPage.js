import React, {Component} from 'react';
import axios from '../../axios-pages';

class MainPage extends Component {
    state={
        title: '',
        content: '',
    };
     loadPage= () =>{
        const name =  this.props.match.params.name;
        axios.get(`${name}.json`).then(response=>{
            this.setState({title: response.data.title, content: response.data.content})
        })
     };

     componentDidMount(){
         this.loadPage();
     };

     componentDidUpdate(prevProps){
         if(this.props.match.params.name !== prevProps.match.params.name){
             this.loadPage();
         }
     }
    render() {
        return (
            <div>
                <h1>{this.state.title}</h1>
                <p>{this.state.content}</p>
            </div>
        );
    }
}

export default MainPage;