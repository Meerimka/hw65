import React, {Component} from 'react';
import axios from "../../axios-pages";
import NavigationItems from "../../components/Navigation/NavigationItems/NavigationItems";


class Edit extends Component {
    state = {
        title :'',
        content :'',
        category: '',
    };

    valueChanged =(event) =>{
        const name = event.target.name;
        this.setState({[name]:event.target.value})
    };



    publishHandler = () => {
        const name=this.state.category;
        const data = {
            title: this.state.title,
            content: this.state.content,
        }
        axios.put(`${name}.json`, data).then(response=>{
            this.props.history.push('/pages/'+name);
        })
    };

    selectChange =event =>{
        const  name = event.target.value;
        axios.get(`${name}.json`).then(response =>{
            this.setState({title: response.data.title, content: response.data.content, category: name})
        })
    }

    render() {
        return (
            <div className='edit'>
                <h2>Edit</h2>
                <div className='edit-box'>
                    <select onChange={(event)=>this.selectChange(event)}>
                        <option value='about'>about</option>
                        <option value='home'>home</option>
                        <option value='contacts'>contacts</option>
                        <option value='products'>products</option>
                    </select>
                    <input className='edit-title' value={this.state.title} onChange={this.valueChanged}
                           type="text" placeholder="Type  title here" name="title"/>
                    <textarea id="editbody" value={this.state.content} onChange={this.valueChanged}
                              cols="30" rows="10" name="content"/>
                    <button className='btn' onClick={this.publishHandler}>Save</button>
                </div>
            </div>
        )
            ;
    }
}


export default Edit;