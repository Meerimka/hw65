import axios from 'axios';

const instance = axios.create({
    baseURL:'https://pages-junusova.firebaseio.com/pages/',
});

export default instance;