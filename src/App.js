import React, { Component } from 'react';
import {Route,Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import './App.css';
import Edit from "./containers/Edit/Edit";
import MainPage from "./containers/MainPage/MainPage";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/pages/admin" exact component={Edit}/>
                    <Route path="/pages/:name" exact component={MainPage}/>
                </Switch>
            </Layout>

        );
    }
}

export default App;
