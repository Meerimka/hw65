import React from 'react';
import './Toolbar.css';
import NavigationItems from "../NavigationItems/NavigationItems";


const Toolbar = () => (
    <header className="Toolbar">

            <nav>
                <NavigationItems>
                </NavigationItems>
            </nav>
    </header>
);
export default Toolbar;